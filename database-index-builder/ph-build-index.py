#!/usr/bin/env python3

# Script to build index for Condition table. Only include the most
# recent records, based on date_measured.
# The index name will be 'idx_farm_condition_latest_a' or
# 'idx_farm_condition_latest_b'. If the index has been existed (but old),
# it will create the new one then drop the old one.

import arrow
import psycopg2

DB_NAME = 'plantinghouse'
INDEX_NAME_TPL = 'idx_farm_condition_latest_{suf}'
STATEMENT_TPL = 'CREATE INDEX {name} ON farm_condition(date_measured DESC NULLS LAST) WHERE date_measured >= %s'
# Time in minutes which we look back to get most recent sensor data
TIME_PAST_MEASURES = 30


def get_existing_index(cur):
    s = "SELECT indexname FROM pg_indexes WHERE tablename = 'farm_condition' AND indexname LIKE 'idx_farm_condition_latest_%'"
    cur.execute(s)
    res = cur.fetchone()
    if res:
        return res[0]


def main():
    conn = psycopg2.connect(database=DB_NAME)
    with conn:
        cur = conn.cursor()
        name_existing = get_existing_index(cur)
        if not name_existing or name_existing.endswith('_b'):
            name_new = INDEX_NAME_TPL.format(suf='a')
        else:
            name_new = INDEX_NAME_TPL.format(suf='b')
        statement_create = STATEMENT_TPL.format(name=name_new)
        print('Create index:')
        print(statement_create)
        time_lower = arrow.get().floor('minute')\
            .replace(minutes=-TIME_PAST_MEASURES).datetime
        cur.execute(statement_create, [time_lower])
        cur.close()

    if name_existing:
        with conn:
            cur = conn.cursor()
            statement_drop = 'DROP INDEX {}'.format(name_existing)
            print('Drop old index:')
            print(statement_drop)
            cur.execute(statement_drop)
    conn.close()


if __name__ == '__main__':
    main()
