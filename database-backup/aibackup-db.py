#!/usr/bin/env python3

# Tool to backup PostgresQL database and upload to Yandex Disk.
# The name of database is passed via command line argument.
# The tool uses Yandex REST API and needs a file ~/db-backup.toml with content:
#     [yandex]
#     access_token = "xxx"
# to retrieve Yandex OAuth access token.
# Other config in the same file:
#     [general]
#     databases = ["name1", "name2"]
#     [influxdb]
#     host = "192.168.8.10"
#     [postgres]
#     host =
#
# We support backing up database from remote server. In that case, we create SSH tunnel
# to the server and do snapshot as if it is in localhost. So, public key authentication
# must be previously setup. The database services also need to skip authentication from localhost.
# For Postgres, it means that you need to configure in pg_hba.conf file.
# How to run:
# aibackup-db.py
# Note: Only work on Python 3.6+

import os
import shutil
import tarfile
import asyncio
import itertools
import zoneinfo
from asyncio import Task
from typing import Dict, BinaryIO, Optional, Sequence, Tuple
from pathlib import PurePosixPath, Path
from datetime import datetime, timedelta
from tempfile import NamedTemporaryFile

import toml
import click
import aiohttp
from aiohttp.client_exceptions import ClientError

YANDEX_PATH = PurePosixPath('Backup/DB/')
CONF_PATH = Path('~/db-backup.toml')
# Mailgun config, used for sending email asking for updating Yandex access token
MAILGUN_URL = 'https://api.mailgun.net/v3/farm.agriconnect.vn/messages'
MAILGUN_API_KEY = os.environ.get('MAILGUN_API_KEY')
VN = zoneinfo.ZoneInfo('Asia/Ho_Chi_Minh')
DEFAULT_INFLUXDB_PORT = 8088
DEFAULT_POSTGRES_PORT = 5432
WAIT_START_SSH = 5
WAIT_SSH_TUNNEL = 5
TIMEOUT_UPLOAD = 60 * 10
RETENTION_DAYS = 7


# Store the future of the processes to open SSH tunnels. It is a dict of {port: future}
ssh_futures = {}


async def inform_wrong_access_key(file_path):
    if not MAILGUN_API_KEY:
        click.echo('Empty MAILGUN_API_KEY. Cannot send email.', err=True)
        return
    msg = '''
    Yandex access_token is not found in {}. Please go to
    https://oauth.yandex.com/authorize?response_type=token&client_id=0ff6bec833334b9cafaa59ecf0ce3c74
    and login as tool@agriconnect.vn to get new access token.
    '''
    message = {
        'from': 'noreply@farm.agriconnect.vn',
        'to': 'ng.hong.quan@gmail.com',
        'subject': 'Please update Yandex access token',
        'text': msg.format(file_path)
    }
    auth = aiohttp.BasicAuth('api', MAILGUN_API_KEY)
    async with aiohttp.ClientSession() as client:
        async with client.post(MAILGUN_URL, auth=auth, data=message) as rsp:
            status = rsp.status
    return status


class YandexClient:
    api_base_url = 'https://cloud-api.yandex.net/v1/disk/'
    access_token = None
    http_client = None

    def __init__(self, access_token: str, loop=None):
        self.access_token = access_token
        headers = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': f'OAuth {access_token}'
        }
        connector = aiohttp.TCPConnector(ssl=False)
        self.http_client = aiohttp.ClientSession(loop=loop, headers=headers, connector=connector)

    def expand_url(self, url: str):
        if url.startswith('/'):
            url = url.lstrip('/')
        return '{}{}'.format(self.api_base_url, url)

    def close(self):
        return self.http_client.close()

    def get(self, url: str, params: Dict = None):
        url = self.expand_url(url)
        return self.http_client.get(url, params=params)

    def put(self, url, params=None, data=None, json=None):
        url = self.expand_url(url)
        return self.http_client.put(url, params=params, data=data, json=json)

    async def exists(self, path: Path):
        params = {
            'path': str(path),
            'fields': 'name,path,type'
        }
        async with self.get('resources', params=params) as res:
            found = res.status == 200
        return found

    async def mkdir(self, path: Path):
        params = {
            'path': str(path)
        }
        async with self.put('resources/', params=params) as res:
            created = res.status == 201
        return created

    async def upload(self, local_path: Path, remote_path: Path):
        params = {
            'path': str(remote_path),
            'fields': 'name'
        }
        async with self.get('resources/upload', params=params) as res:
            if res.status != 200:
                click.echo('Failed to request upload URL. Status {}'.format(res.status), err=True)
                return False
            jres = await res.json()
        try:
            upload_url = jres['href']
        except KeyError:
            error = jres.get('description', '<Some error>')
            click.echo(f'Failed to upload. Error: {error}', err=True)
            return False
        try:
            async with self.http_client.put(upload_url, data=open(local_path, 'rb')) as res:
                success = res.status == 201
        except ClientError as e:
            click.echo(f'Upload failed with error {e}', err=True)
            return False
        return success

    async def delete(self, path: Path):
        params = {
            'path': str(path),
            'permanently': 'true',
        }
        url = self.expand_url('resources')
        try:
            async with self.http_client.delete(url, params=params) as res:
                click.echo(f'Delete response: {res.status}')
        except ClientError as e:
            click.echo(f'Failed to request. Error {e}', err=True)
            return False


def create_ssh_tunnels(config: dict, loop) -> Dict[str, Task]:
    cmd_parts = ['ssh', '-o', 'ExitOnForwardFailure yes', '-N']
    try:
        host_if = config['influxdb']['host']
    except KeyError:
        host_if = ''
    try:
        host_pg = config['postgres']['host']
    except KeyError:
        host_pg = ''
    if host_if == 'localhost':
        host_if = ''
    if host_pg == 'localhost':
        host_pg = ''
    if not host_if and not host_pg:
        return {}
    if host_if == host_pg:
        cmd_parts.extend(('-L', f'{DEFAULT_INFLUXDB_PORT}:localhost:{DEFAULT_INFLUXDB_PORT}'))
        cmd_parts.extend(('-L', f'{DEFAULT_POSTGRES_PORT}:localhost:{DEFAULT_POSTGRES_PORT}'))
        cmd_parts.append(host_if)
        click.echo("Create SSH tunnel with command: {}".format(' '.join(cmd_parts)))
        future = loop.create_task(asyncio.create_subprocess_exec(*cmd_parts, loop=loop))
        return {
            DEFAULT_INFLUXDB_PORT: future,
            DEFAULT_POSTGRES_PORT: future
        }
    out = {}
    if host_if:
        cmd_1 = cmd_parts + ['-L', f'{DEFAULT_INFLUXDB_PORT}:localhost:{DEFAULT_INFLUXDB_PORT}', host_if]
        click.echo("Create SSH tunnel with command: {}".format(' '.join(cmd_1)))
        out[DEFAULT_INFLUXDB_PORT] = loop.create_task(asyncio.create_subprocess_exec(*cmd_1, loop=loop))
    if host_pg:
        cmd_2 = cmd_parts + ['-L', f'{DEFAULT_POSTGRES_PORT}:localhost:{DEFAULT_POSTGRES_PORT}', host_pg]
        click.echo("Create SSH tunnel with command: {}".format(' '.join(cmd_2)))
        out[DEFAULT_POSTGRES_PORT] = loop.create_task(asyncio.create_subprocess_exec(*cmd_2, loop=loop))
    return out


async def wait_for_ssh_tunnel(ssh_future: Task, port: int) -> bool:
    counter = itertools.count()
    while next(counter) < WAIT_START_SSH:
        await asyncio.sleep(1)
        if ssh_future.done():
            break
    ssh_process = ssh_future.result()
    if ssh_process.returncode is not None:   # Not terminated yet
        # SSH already exited, maybe due to failure
        click.echo(f"SSH tunnel for port {port} failed.", err=True)
        return False
    # It needs a while for SSH to establish the tunnel
    counter = itertools.count()
    while next(counter) < WAIT_SSH_TUNNEL:
        try:
            reader, writer = await asyncio.open_connection('localhost', port)
        except ConnectionRefusedError:
            await asyncio.sleep(1)
            continue
        # Ok. Can connect.
        writer.close()
        return True
    return False


async def dump_rel_database(db_name: str, ssh_future: Task, port: int) -> BinaryIO:
    # In case that Postgres server is remote, we test if SSH tunnel is established
    if ssh_future is not None:
        ready = await wait_for_ssh_tunnel(ssh_future, port)
        if not ready:
            return
    db_file = NamedTemporaryFile()
    # Implement the command `pg_dump -OFc db_name | gzip -c > /tmp/somefile`
    # We use Postgres custom format for output file.
    # Ref: https://stackoverflow.com/a/36666420
    read_fd, write_fd = os.pipe()
    cmds = ['pg_dump', '-OFc', db_name]
    if ssh_future:
        cmds.extend(('-h', 'localhost'))
    click.echo('Dump relational DB with command: {}'.format(' '.join(cmds)))
    await asyncio.create_subprocess_exec(*cmds, stdout=write_fd)
    os.close(write_fd)
    pgz = await asyncio.create_subprocess_exec('gzip', '-c', stdin=read_fd, stdout=db_file)
    os.close(read_fd)
    await pgz.wait()
    return db_file


async def dump_ts_database(db_name, ssh_future, port) -> Optional[Path]:
    # In case that InfluxDB server is remote, we test if SSH tunnel is established
    if ssh_future is not None:
        ready = await wait_for_ssh_tunnel(ssh_future, port)
        if not ready:
            return
    # Time-series database is dumped to /tmp/ts_name_20170130-000000
    now = datetime.now(tz=VN)
    folder_path = Path(f'/tmp/ts_{db_name}_{now:%Y%m%d-%H%M%S}')
    if folder_path.exists():
        shutil.rmtree(folder_path)
    os.mkdir(folder_path)
    # Ref: https://docs.influxdata.com/influxdb/v1.5/administration/backup_and_restore/
    cmd = ('influxd', 'backup', '-portable', '-database', db_name, str(folder_path))
    click.echo('Backup time-series database with command: {}'.format(' '.join(cmd)))
    p_dump = await asyncio.create_subprocess_exec(*cmd)
    await p_dump.wait()
    if p_dump.returncode != 0:
        # Failed to dump database
        return
    # Done dump. Compress
    tarpath = folder_path.with_suffix('.tar')
    with tarfile.open(tarpath, 'w') as tar:
        tar.add(folder_path)
    # Remove dump folder
    shutil.rmtree(folder_path)
    return tarpath


async def create_remote_directory(client, path: str) -> bool:
    # `path` will be db_name/year/month/day/
    # `real_path` will be Backup/DB/db_name/year/month/day/
    real_path = YANDEX_PATH / path
    click.echo(f'> Does {real_path} exist? ')
    # First we test if Backup/DB/db_name/year/month/day/ exist.
    exist = await client.exists(real_path)
    click.echo(f' > {exist}')
    if exist:
        return True
    # If not, we create the subfolders
    skip_check = False
    levels = list(reversed(path.parents))[1:] + [path]
    created = False
    for p in levels:
        sub = YANDEX_PATH / p
        # Checking for directory existence is slow, so we don't have to
        # check the subpaths after creating
        exist = await client.exists(sub) if not skip_check else False
        if exist:
            continue
        click.echo(f'> Create {sub} ', nl=False)
        created = await client.mkdir(sub)
        click.echo('SUCCESS' if created else 'FAILED')
        if not created:
            break
        skip_check = True
    return created


def get_database_names(config) -> Tuple[str]:
    try:
        return config['general']['databases']
    except KeyError:
        return ()


async def backup_single(db_name: str, client: YandexClient) -> bool:
    now = datetime.now(tz=VN)
    # The dump file is placed under subdirectory name/2016/01/30
    subdir = PurePosixPath(f'{db_name}/{now:%Y/%m/%d/}')
    # Create directory on Yandex Disk
    task_mkdir = asyncio.create_task(create_remote_directory(client, subdir))
    # Dump database to /tmp/
    click.echo(f'> Dump database {db_name}')
    try:
        ssh_future_postgres = ssh_futures[DEFAULT_POSTGRES_PORT]
    except KeyError:
        ssh_future_postgres = None
    task_dump_db = asyncio.create_task(dump_rel_database(db_name, ssh_future_postgres, DEFAULT_POSTGRES_PORT))
    done, pending = await asyncio.wait((task_mkdir, task_dump_db),
                                       timeout=TIMEOUT_UPLOAD)
    if pending:
        click.echo('> Some tasks cannot finish.')
        return
    remote_dir_created = task_mkdir.result()
    if not remote_dir_created:
        click.echo('> Failed to create remote directory. Stop.', err=True)
        return False
    db_file = task_dump_db.result()
    # Upload to Yandex Disk
    # Upload relational database
    filepath = db_file.name
    # The dump file will be named like `name_20160130-000000.pgd.gz`
    remote_name = f'{db_name}_{now:%Y%m%d-%H%M%S}.pgd.gz'
    remote_path = YANDEX_PATH / subdir / remote_name
    click.echo(f'> Upload {filepath} to {remote_path}')
    task_upload_db = asyncio.create_task(client.upload(filepath, remote_path))
    tasks = [task_upload_db]
    for i in range(RETENTION_DAYS, 31):
        old = now - timedelta(days=i)
        remote_path_old = YANDEX_PATH / f'{db_name}/{old:%Y}/{old:%m}/{old:%d}'
        click.echo(f'> Delete {i} days old files: {remote_path_old}')
        task_delete_old = asyncio.create_task(client.delete(remote_path_old))
        tasks.append(task_delete_old)
    await asyncio.wait(tasks, timeout=TIMEOUT_UPLOAD)
    click.echo(f'Done {db_name}.')
    return True


async def backup_multiple(db_names: Sequence[str], access_token: str):
    client = YandexClient(access_token)
    jobs = [asyncio.create_task(backup_single(db_name, client)) for db_name in db_names]
    await asyncio.wait(jobs)
    await client.close()


@click.command()
def main():
    file_path = CONF_PATH.expanduser()
    with open(file_path) as f:
        config = toml.load(f)
    try:
        access_token = config['yandex']['access_token']
    except KeyError:
        click.echo('Failed to read access token.', err=True)
        loop = asyncio.get_event_loop()
        asyncio.run_until_complete(inform_wrong_access_key(file_path))
        loop.close()
        return
    db_names = get_database_names(config)
    if not db_names:
        click.echo('No database to backup.')
        return
    loop = asyncio.get_event_loop()
    # If database is in remote server, call SSH to create tunnel
    ssh_futures.update(create_ssh_tunnels(config, loop))
    # Do backup
    loop.run_until_complete(backup_multiple(db_names, access_token))
    # Backup is done, close SSH tunnel
    for ssh_future in ssh_futures.values():
        if ssh_future.done():
            ssh_process = ssh_future.result()
            if ssh_process.returncode is None:
                click.echo(f'There is SSH running {ssh_process}. Stop it.')
                ssh_process.terminate()
    # Wait a little for the processes to be terminated
    loop.run_until_complete(asyncio.sleep(1))
    loop.close()
    click.echo('Done. Bye!')


if __name__ == '__main__':
    main()
