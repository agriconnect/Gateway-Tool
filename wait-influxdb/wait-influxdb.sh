#!/bin/sh

# Script to wait until InfluxDB server is ready to connect. The timeout is 5s.

HOST=${1:-'localhost'}
PORT=${2:-8086}

for try in 1 2 3 4 5; do
	nc -z $HOST $PORT

	infoff=$?

	if [ $infoff -eq 0 ]; then
		# InfluxDB is online
		break
	fi

	sleep 1
done
exit $infoff
